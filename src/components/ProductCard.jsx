import React, { Component } from "react";
import ReactStars from "react-rating-stars-component";
import PropTypes from 'prop-types';
import ReactDOM from "react-dom";

export class ProductCard extends Component {
  ratingChanged = newRating => {
    console.log(newRating);
  };
  render() {
    const { src, title, artist, description, price, handleClick } = this.props;
    return (
      <div>
        <div className="about">
          <img src={src} alt="" />
          <div className="product">
            <div className="title">
              {title}
              <span>
                <i> {artist}</i>
              </span>
            </div>
            <ReactStars
              count={5}
              onChange={this.ratingChanged}
              size={24}
              color2={"#d23939"}
            />
          </div>
          <p className="product">{description}</p>
          <div className="price-cart-btn product">
         <span>${price}</span> <button onClick={handleClick}>ADD TO CART</button>
        </div>
        </div>
        
      </div>
    );
  }
}
