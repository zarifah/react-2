import React, { Component } from "react";
import ReactDOM from 'react-dom';
import {ProductCard}  from "./ProductCard";
import  {Cart}  from "./Cart";
import {getMusics}  from "../API/fetchAPI";
import axios from 'axios';
import ls from 'local-storage';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faArrowRight,
  faArrowLeft
   } from "@fortawesome/free-solid-svg-icons";
import seperator from "../image/line/line.png";
export class ProductList extends Component {

  constructor() {
    super();
    this.state = {
      data: {
        count: 0,
        musics: []
      },
      card: [],
      isactive: false
    };
  }

  getdata = async () => {
    const answer = await  (getMusics());
    this.setState({ data: { musics: [...answer] } });
    console.log(this.state.data.musics);
  };

  componentDidMount() {
    this.getdata();
  }

  removeFromCart = (id, count) => {
  
    if (count > 1) {
        this.setState({cart : this.state.cart.map(item => {
            if (item.id === id) {
                return {
                    ...item,
                    count: item.count - 1
                }
            }
            return item
        })})
    } else {
        this.setState({cart : this.state.cart.filter(item => item.id !== id)})
    }
}
  AddToCart = (imgPath, title, rate, cost, id) => {
    console.log("Add to cart begin");
    
    const isAvailable = this.state.cart.find(item => id === item.id);
    if (isAvailable) {
      this.setState(
    {    cart:
        this.state.cart.map(item => {
          if (item.id === id) { 
           
            console.log({...item, count: item.count + 1},"Inside if isavailable");
            return {...item, count: item.count + 1};
          }
          return item;
        })
      }
      );
   
    } else {
      this.setState({
        cart: [
          ...this.state.cart,
          {
            imgPath,
            title,
            rate,
            cost,
            id,
            count: 1
          }
        ]
      });
    }

    this.setState({ isactive: true });
  };
 
  render() {
    console.log(window.location.origin);
    return (
      <section className="arrivals">
        <div className="container">
          <div className="arrivals-header">
            <h1> LATEST ARRIVALS IN MUSICA</h1>
            <img src={seperator} alt="Logo" className="img-btn" />
            <div className="buttons">
              <button className="arrows-btn">
              <FontAwesomeIcon icon={faArrowLeft} size="x" round={true} color="white"/>
              </button>
              <button className="arrows-btn">
              <FontAwesomeIcon icon={faArrowRight} size="x" round={true} color="white" />
              </button>
            </div>
          </div>
          {this.state.isactive &&
            this.state.cart.map(
              ({ imgPath, title, rate, price, count, id }) => (
                <Cart
                  key={id}
                  cart={this.state.cart}
                  src={window.location.origin + { imgPath }}
                  rate={rate}
                  title={title}
                  cost={price}
                  count={count}
                  removeFromCart={this.removeFromCart}
                />
              )
            )}
          <ul className="product-list">
            {this.state.data.musics.map(
              ({ imgPath, id, title, rate, price, description, artist }) => (
                <li key={id} className="products-item">
                  <ProductCard
                    key={id}
                    isactive={this.props.activetab}
                    src={imgPath}
                    title={title}
                    artist={artist}
                    description={description}
                    price={price}
                    handleClick={() =>
                      this.AddToCart(imgPath, title, rate, price, id)
                    }
                    
                  />
 
   
                </li>
              )
            )}
          </ul>
        </div>
      </section>
    );
  }
}
