import React, { useState, useEffect, Component } from "react";
import "./../styles/style.sass";
import "./../styles/Menu.scss";


export class Menu extends Component {
  constructor() {
    super();
    this.state = {
      
      isActive: false
    };
  }
  setActive = () => {
    this.setState({ isActive: true});
  };

 
  render() {
        
    let linksMarkup = this.props.links.map((link, index) => {
        let linkMarkup = link.active ? (
            <a className="menu__link menu__link--active" href={link.link}>{link.label}</a>
        ) : (
            <a className="menu__link" href={link.link}>{link.label}</a>
        );

        return (
            <li key={index} className="menu__list-item">
                {linkMarkup}
            </li>
        );
    });

    return (
        <nav className="menu">
            <h2 style={{
            backgroundImage: 'url(' + this.props.logo + ')'
            }} className="menu__logo">
            <h1 className="logo">M</h1>
             </h2>
            <h1 className="logo2">Store</h1>
               

            <div className="menu__right">
                <ul className="menu__list">
                    {linksMarkup}
                </ul>

               
            </div>
        </nav>
    );
}
}
export default Menu;