import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
  root: {
    maxWidth: 270,
  },
  media: {
    height: 160,
  },
});

export default function MediaCard() {
  
  const classes = useStyles();

  return (
  
    <Card className={classes.root}>
        
      <CardActionArea> 
        <CardMedia
          className={classes.media}
          image="../images/Layer1.png"
          title="Deer"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            Oh my Deer
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Impedit quo fugit officia adipisci iure fuga 
            facilis quasi deserunt, 
            
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <h3>$14.99</h3>
        <Button class="btn" size="small" >
          Add to cart
        </Button>
      </CardActions>
    </Card>
    
  );
}