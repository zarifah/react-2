import React from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faFacebook,
    faTwitter,
    faDribbble,
    faVimeo,
      
  } from "@fortawesome/free-brands-svg-icons";
  import {faShoppingCart,
    faEnvelope,
     } from "@fortawesome/free-solid-svg-icons";

   export default function Header(){
       return (
           <div className="social-container">
               
 <a
  href="https://www.facebook.com/learnbuildteach/"
  className="facebook social"
>
  <FontAwesomeIcon icon={faFacebook} size="x" />
</a>
<a
  href="https://dribbble.com/"
  className="dribble social"
>
  <FontAwesomeIcon icon={faDribbble} size="x" round={true} />
</a>
<a href="https://www.twitter.com/jamesqquick"
 className="twitter social">
  <FontAwesomeIcon icon={faTwitter} size="x" />
</a>
<a
  href="https://telegram.org/"
  className="mail social"
>
  <FontAwesomeIcon icon={faEnvelope} size="x" />
</a>
<a
  href="https://vimeo.com/"
  className="vimeo social"
>
  <FontAwesomeIcon icon={faVimeo} size="x" round={true} />
</a>
<button className="cart" > 
<FontAwesomeIcon icon={faShoppingCart} size="x" /> Cart</button>
<a href="#" className="login">
    Login/Register </a>
    
</div>

       )
   }