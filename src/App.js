 import React, { Component } from 'react';
 import Header from './components/Header';
 import Menu from './components/Menu';
 import {Musica} from './components/WelcomeSection';
 import { ProductList } from "./components/ProductList";
 import {ProductCard} from "./components/ProductCard";

 import logo from './image/logo.png';
 
 export default class App extends Component {
  render() { 
    let links = [
      { label: 'Home', link: '#home', active: true },
      { label: "CD's", link: '#cd' },
      { label: "DVD's", link: '#dvd' },
      { label: "News", link: '#news' },
      { label: "Portfolio", link: '#portfolio' },
      { label: 'Contact Us', link: '#contact-us' },
    ];

   
  return (
    <div className="App">
      <Header/>
     <Menu links={links} logo={logo} />
    <Musica/>
    <ProductList/>
    <ProductCard/>
    
    </div>
  );
}
}

